#!/usr/bin/env bash

declare -A arrValidManifests
validEnvironments=("dev" "stg" "prod")

kubectl get nodes | grep minikube && ENVIRONMENT=dev
kubectl get nodes | grep stg && ENVIRONMENT=stg
kubectl get nodes | grep irv-prod && ENVIRONMENT=prod

# Collect the file paths into an array using 'ls'
k_files=$(find kustomize -name kustomization.yaml | grep overlays)

# Loop through the array and assign the wildcards to bash variables using 'awk'
OLD_IFS=$IFS
for k_file in $k_files; do
  IFS='/' read -ra subdirs <<< "$k_file"
  k_app="${subdirs[1]}"
  k_env="${subdirs[3]}"
  
  case $k_env in
    dev|stg|prod)
      arrValidManifests[$k_env]+="$k_app "
      ;;
    *)
      echo "Unknown environment: $k_env"
      ;;
  esac
done
IFS=$OLD_IFS

FORCE=0
function usage () {
  cat << EOF
USAGE:

./$(basename $0) [-h] [-m CHART_VERSION] [-n NAMESPACE] -e ENVIRONMENT

OPTIONS:
  -h             --> shows usage"
  -e ENVIRONMENT --> Environment to deploy to (MUST match your KUBECONFIG or ~/.kub/config)
  -c CHART_LIST  --> Comma Seperated list of specific components to install
                     (Default: ${arrValidManifests[*]})
  -f             --> Force -- REALLY run this. Will not run without this flag

EXAMPLES:
  ./$(basename $0) -e stg
  ./$(basename $0) -e stg -c app-to-deploy

$1
EOF
}

echo

# list of arguments expected in the input
optstring=":he:fc:l"
while getopts ${optstring} arg; do
  case ${arg} in
    h)
      echo "showing usage!"
      usage
      exit 1
      ;;
    f)
      FORCE=1
      ;;
    e)
      TARGET_ENVIRONMENT=${OPTARG}
      ;;
    c)
      arrManifests=()
      for component in ${OPTARG//,/ }
      do
          in_array=$(echo ${arrValidManifests[$ENVIRONMENT]} | grep -ow "$component" | wc -w)
          if [ ${in_array} -eq 0 ]; then
            usage
            echo
            echo "$0: '${component}' is not one of the following valid components:"
            echo "    ${arrValidManifests[$ENVIRONMENT]}"
            echo
            exit 1
          fi

          arrManifests+=($component)
      done
      ;;
    l)
      echo "Listing all of the valid components using kustomization:" 
      for k_env in "${!arrValidManifests[@]}"; do
        echo "$k_env kustomization components:"
        for component in "${arrValidManifests[$k_env]}"; do
          echo "  $component"
        done
        echo
      done
      echo
      exit 0
      ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

[[ -z $TARGET_ENVIRONMENT ]] && usage 'environment option (-e) is required'

# Make certain that the supplied env matches the kubectl configured env
if [[ "${TARGET_ENVIRONMENT}" != "${ENVIRONMENT}" ]]; then
 usage
  echo
  echo "$0: You supplied '${TARGET_ENVIRONMENT}'  on the command line."
  echo "    This does not match your current kubeconfig settings for '${ENVIRONMENT}'"
  echo "    Confirm .kube/config and KUBECONFIG match the env you intend to deploy."
  echo
  exit 1
fi

# Install everything if it's not provided
if [ ${#arrManifests[@]} -eq 0 ]; then
  echo "Install Everything for ${TARGET_ENVIRONMENT}"
  arrManifests=(${arrValidManifests["$TARGET_ENVIRONMENT"]})
else
  echo "Installing ${arrManifests[@]}"
fi

echo "${ENVIRONMENT}"
for element in "${arrManifests[@]}"; do
  echo "  $element"
done

if [ $FORCE -ne 1 ]; then
  read -p "Press enter to continue, ctrl-c to exit."
fi

counter=2
while [ $counter -gt 0 ]
do
   echo -ne "\033[1K${counter}"
   counter=$(( $counter - 1 ))
   sleep 1
done
echo -e "\033[2K"

for component in "${arrManifests[@]}"; do
  component=$(echo "$component" | tr -d '[:space:]')

  printf "\n----- Install ${component} -----\n"
  # Install application
  echo "kubectl apply -k kustomize/${component}/overlays/${ENVIRONMENT}"
  kubectl apply -k kustomize/${component}/overlays/${ENVIRONMENT}

  # Check for any secrets in the overlays directory for this component
  secret_dir="kustomize/${component}/overlays/${ENVIRONMENT}/secrets"
  if [[ -d "$secret_dir" && $(find "$secret_dir" -type f -name "*.yaml" -print -quit) ]]; then
    for secret in $(find "$secret_dir" -type f -name "*.yaml" -print); do
      echo "Applying ${secret}"
      sops -d ${secret} | kubectl apply -f -
    done
  fi

  # Check for any env specific manifests
  manifest_dir="kustomize/${component}/overlays/${ENVIRONMENT}/manifests"
  if [[ -d "$manifest_dir" && $(find "$manifest_dir" -type f -name "*.yaml" -print -quit) ]]; then
    for manifest in $(find "$manifest_dir" -type f -name "*.yaml" -print); do
      echo "Applying ${manifest}"
      kubectl apply -f ${manifest}
    done
  fi

done
