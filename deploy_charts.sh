#!/usr/bin/env bash

declare -A arrValidCharts
validEnvironments=("dev" "stg" "prod")

kubectl get nodes | grep minikube && ENVIRONMENT=dev
kubectl get nodes | grep stg && ENVIRONMENT=stg
kubectl get nodes | grep irv-prod && ENVIRONMENT=prod

# Collect the file paths into an array using 'ls'
k_files=$(find charts -name values.yaml)

# Loop through the array and assign the wildcards to bash variables using 'awk'
OLD_IFS=$IFS
for k_file in $k_files; do
  IFS='/' read -ra subdirs <<< "$k_file"
  k_app="${subdirs[1]}"
  k_env="${subdirs[2]}"

  case $k_env in
    dev|stg|prod)
      arrValidCharts[$k_env]+="$k_app "
      ;;
    *)
      echo "Unknown environment: $k_env"
      ;;
  esac
done
IFS=$OLD_IFS

FORCE=0
function usage () {
  cat << EOF
USAGE:

./$(basename $0) [-h] [-m CHART_VERSION] [-n NAMESPACE] -e ENVIRONMENT

OPTIONS:
  -h             --> shows usage"
  -e ENVIRONMENT --> Environment to deploy to (MUST match your KUBECONFIG or ~/.kub/config)
  -c CHART_LIST  --> Comma Seperated list of specific components to install
                     (Default: ${arrValidCharts[*]})
  -f             --> Force -- REALLY run this. Will not run without this flag

EXAMPLES:
  ./$(basename $0) -m ${VAULT_VERSION}] -n ${NAMESPACE}
  ./$(basename $0) -c vault
  ./$(basename $0) -c vault,anotherhelmchart -f
  ./$(basename $0) -e stg

$1
EOF
}

echo

# list of arguments expected in the input
optstring=":he:fc:l"
while getopts ${optstring} arg; do
  case ${arg} in
    h)
      echo "showing usage!"
      usage
      exit 1
      ;;
    f)
      FORCE=1
      ;;
    e)
      TARGET_ENVIRONMENT=${OPTARG}
      ;;
    c)
      arrCharts=()
      for component in ${OPTARG//,/ }
      do
          in_array=$(echo ${arrValidCharts[$ENVIRONMENT]} | grep -ow "$component" | wc -w)
          if [ ${in_array} -eq 0 ]; then
            usage
            echo
            echo "$0: '${component}' is not one of the following valid components:"
            echo "    ${arrValidCharts[$ENVIRONMENT]}"
            echo
            exit 1
          fi

          arrCharts+=($component)
      done
      ;;
    l)
      echo "Listing all of the valid components using kustomization:" 
      for k_env in "${!arrValidCharts[@]}"; do
        echo "$k_env kustomization components:"
        for component in "${arrValidCharts[$k_env]}"; do
          echo "  $component"
        done
        echo
      done
      echo
      exit 0
      ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

[[ -z $TARGET_ENVIRONMENT ]] && usage 'environment option (-e) is required'

# Make certain that the supplied env matches the kubectl configured env
if [[ "${TARGET_ENVIRONMENT}" != "${ENVIRONMENT}" ]]; then
 usage
  echo
  echo "$0: You supplied '${TARGET_ENVIRONMENT}'  on the command line."
  echo "    This does not match your current kubeconfig settings for '${ENVIRONMENT}'"
  echo "    Confirm .kube/config and KUBECONFIG match the env you intend to deploy."
  echo
  exit 1
fi

# Install everything if it's not provided
if [ ${#arrCharts[@]} -eq 0 ]; then
  echo "Install Everything for ${TARGET_ENVIRONMENT}"
  echo "${arrValidCharts["$TARGET_ENVIRONMENT"]}"
  arrCharts=(${arrValidCharts["$TARGET_ENVIRONMENT"]})
  echo "$arrCharts"
else
  echo "Installing ${arrCharts[@]}"
fi

case "$ENVIRONMENT" in
  dev|stg|prod)
    ;;
  *)
    echo "$0: Environment (-e) must be dev, stg, or prod" >&2
    exit 1
    ;;
esac

echo "----- Confirm these are the correct nodes -----"
echo "ENVIRONMENT -----> ${ENVIRONMENT}"
kubectl get nodes
echo "----- Installing the following packages"

echo "${ENVIRONMENT}"
for element in "${arrCharts[@]}"; do
  echo "  $element"
done

if [ $FORCE -ne 1 ]; then
  read -p "Press enter to continue, ctrl-c to exit."
fi

counter=2
while [ $counter -gt 0 ]
do
   echo -ne "\033[1K${counter}"
   counter=$(( $counter - 1 ))
   sleep 1
done
echo -e "\033[2K"

# Search for subdirs in the chart directory and deploy the chart and components
# dev_arrValidCharts+=($(find ./charts/ -type d -name dev | sed 's/\/dev//g' | xargs -n 1 basename))
for chart in "${arrCharts[@]}"; do

  # Reset variables between loops
  unset NAMESPACE
  unset CHART_VERSION
  DIR_NAME=./charts/${chart}/${ENVIRONMENT}/

  printf "\n----- Install $chart -----\n"

  # Load env if available (could contain version and namespace)
  if [[ -n $(ls "${DIR_NAME}"/*.env 2>/dev/null) ]]; then
    for envfile in $(find ${DIR_NAME}/*.env); do
      echo "Reading $envfile"
      source ${envfile}
    done
  fi

  if [ -z "$NAMESPACE" ] || [ -z "$CHART_VERSION" ]; then
    echo "Namespace or Chart Version is empty or undefined for ${chart}, SKIPPING..."
    echo "     NAMESPACE: ${NAMESPACE}"
    echo " CHART_VERSION: ${CHART_VERSION}"
    echo "      DIR_NAME: ${DIR_NAME}"
    continue
  else
    echo "     NAMESPACE: ${NAMESPACE}"
    echo " CHART_VERSION: ${CHART_VERSION}"
    echo "      DIR_NAME: ${DIR_NAME}"
  fi

  # Create the namespace here so related manifests and secrets can be applied
  if [ -v NAMESPACE ] ; then
    kubectl get ns ${NAMESPACE} > /dev/null 2>&1
    if [ $? -eq "1" ]; then
      echo -n "Creating namespace ${NAMESPACE}: "
      kubectl create ns ${NAMESPACE}
    else
      echo "NAMESPACE '${NAMESPACE}' already exists ... not creating it again" 
    fi
  fi

  # Deploys secrets in charts/CHARTNAME/ENVIRONMENT/secrets
  if [[ -d "./charts/${chart}/${ENVIRONMENT}/secrets" && $(ls ./charts/${chart}/${ENVIRONMENT}/secrets/*.enc.yaml | wc -l) -gt 0 ]]; then
    for secret in $(ls ./charts/${chart}/${ENVIRONMENT}/secrets/*.enc.yaml); do
      echo "Applying secret ${secret} for chart ${chart}"
      sops -d ${secret} | kubectl apply -f -
    done
  else
    echo "No secrets for chart ${chart}"
  fi

  # Deploy related manifests (eg. ingressRoute)
  # charts/CHARTNAME/ENVIRONMENT/manifests
  if [[ -d "charts/${chart}/${ENVIRONMENT}/manifests" && $(ls ./charts/${chart}/${ENVIRONMENT}/manifests/*.yaml | wc -l) -gt 0 ]]; then
    for manifest in $(ls ./charts/${chart}/${ENVIRONMENT}/manifests/*yaml); do
      echo "Applying manifest ${manifest} for chart ${chart}"
      cat ${manifest} | kubectl apply -f -
    done
  else
    echo "No additional manifests for chart ${chart}"
  fi

  # Use the custom script if provided. Preferably, do the work here
  # If you create a deplot_chart.sh script, make sure it cannot
  # run independently since this script has checks for correct env/nodes
  if [[ -f ${DIR_NAME}/deploy_chart.sh ]]; then
    echo "Executing: ${ENVIRONMENT}/charts/${chart}/deploy_chart.sh"
    bash ${DIR_NAME}/deploy_chart.sh
  else
    source ${DIR_NAME}/*.env
    if [ -z "$REPO_NAME" ] || [ -z "$REPO_URL" ] || [ -z "$CHART_NAME" ] || [ -z "$CHART_VERSION" ] || [ -z "$NAMESPACE" ] || [ -z "$DIR_NAME" ]; then
      echo "One or more required variables are not defined in env file."
      exit 1
    fi
    helm repo add ${REPO_NAME} ${REPO_URL}
    helm repo update
    echo helm upgrade ${CHART_NAME} ${REPO_NAME}/${CHART_NAME} -i --version ${CHART_VERSION} \
      --create-namespace -n ${NAMESPACE} \
      -f ${DIR_NAME}/values.yaml
    helm upgrade ${CHART_NAME} ${REPO_NAME}/${CHART_NAME} -i --version ${CHART_VERSION} \
      --create-namespace -n ${NAMESPACE} \
      -f ${DIR_NAME}/values.yaml
  fi

  printf "\n---- Chart $chart deployment COMPLETED ----\n"
done
