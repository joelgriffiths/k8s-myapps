Project Overview
================

This README details the project's directory structure, encompassing configuration files, deployment scripts, and Kubernetes resources. The project is set up to manage and deploy various services using Helm and Kustomize, with a focus on environment-specific configurations and security through encrypted secrets.

Directory Structure
-------------------


```
├── charts                      # Helm charts for deploying various services
│   ├── external-dns            # Helm chart for ExternalDNS
│   │   └── dev                 # Development specific configurations for ExternalDNS
│   │       ├── default.env     # Default environment variables
│   │       ├── secrets
│   │       │   └── rndc-key-secret.enc.yaml # Encrypted DNS key secret using sops
│   │       ├── values-default.yaml # Default values for the Helm chart
│   │       └── values.yaml     # Custom values for the Helm chart
│   ├── chart2                    # Helm chart for Keel
│   │   └── dev
│   │       ├── default.env     # Default environment variables
│   │       ├── secrets
│   │       │   └── secrets-enc.yaml # Encrypted secrets using sops
│   │       └── values.yaml     # Custom values for the Helm chart
│   │   └── stg
│   │       ├── default.env     # Default environment variables
│   │       ├── secrets
│   │       │   └── secrets-enc.yaml # Encrypted secrets using sops
│   │       └── values.yaml     # Custom values for the Helm chart
│   └── chart3                 # Helm chart for MetalLB
│       └── prod
│           ├── default.env     # Default environment variables
│           ├── manifests
│           │   ├── address-pool.yaml # Address pool configuration
│           │   └── l2advertisement.yaml # Layer 2 advertisement configuration
│           ├── secrets
│           │   └── secret.enc.yaml # Encrypted secrets using sops
│           └── values-disable.yaml # Values to disable certain features
├── kustomize                   # Kustomize configurations for managing Kubernetes resources
│   └── service-setup           # Kustomize setup for a particular service
│       ├── base                # Base resources for the service
│       └── overlays            # Overlays for customizing the base resources in different environments
│           ├── dev             # Development environment specific configurations
│           │   ├── manifests   # Flat Kubernetes manifests
│           │   └── secrets     # Encrypted secrets using sops```:w
├── deploy_charts.sh            # Script to deploy Helm charts across environments
├── deploy_kustomize.sh         # Script to apply configurations using Kustomize


Deployment Scripts
------------------

### deploy_charts.sh

This script is designed to facilitate the deployment of Helm charts across different Kubernetes environments:

-   Options:
    -   `-h`: Displays usage information.
    -   `-m CHART_VERSION`: Specifies the version of Helm charts to be deployed.
    -   `-n NAMESPACE`: Sets the Kubernetes namespace for deployment.
    -   `-e ENVIRONMENT`: Targets the deployment to a specific environment, aligning with kubeconfig context.
    -   `-c CHART_LIST`: Allows specification of a comma-separated list of Helm charts to deploy. Default is `keel, external-dns`.
    -   `-f`: A force flag that must be included to execute the script, ensuring intentional operations.

Examples:

-   Display usage: `./deploy_charts.sh -h`
-   Deploy charts in the 'dev' environment: `./deploy_charts.sh -e dev -f`
-   Deploy specific charts in a named environment: `./deploy_charts.sh -e dev -c external-dns,keel -f`

### deploy_kustomize.sh

This script applies Kubernetes configurations using Kustomize, similarly requiring specification of an environment and the force flag for execution.

Usage and Security
------------------

-   Environment-Specific Configurations: Vital for segregating deployment settings and ensuring tailored deployment across different environments.
-   Security through Encrypted Secrets: Utilizes `sops` for encryption to enhance security, maintaining confidentiality and integrity of sensitive data.

Conclusion
----------

The project structure and deployment scripts are instrumental for deploying and managing consistent environments across different Kubernetes clusters, ensuring both efficiency and security.